import java.util.*;

class Demo{

	int elem(int arr[]){
		HashMap<Integer,Integer> hm=new HashMap<>();
		int index=Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			if(hm.containsKey(arr[i])){
				if(hm.get(arr[i]) < index){
					index=hm.get(arr[i]);
				}
			}
			else{
				hm.put(arr[i],i);
			}
		}
		if(index==Integer.MAX_VALUE){
			return -1;
		}
		else{
			return index+1;
		}
	}

	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		Demo obj=new Demo();

		System.out.println("Enter size of array:");
		int n=sc.nextInt();
		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.elem(arr);

		System.out.println(ret);
		
	}
}

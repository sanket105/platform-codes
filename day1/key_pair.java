import java.util.*;

class Demo{
	boolean ask(int arr[],int sum){
		Arrays.sort(arr);
		int i=0;
		int j=arr.length-1;
		while(i<j){
			if((arr[i]+arr[j])>sum){
				j--;
			}
			else if((arr[i]+arr[j])<sum){
				i++;
			}
			else if((arr[i]+arr[j])==sum){
				return true;
			}
			else{
				return false;
			}
		}

		return false;
	}

	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		Demo obj=new Demo();
		
		System.out.println("Enter size of array:");
		int n=sc.nextInt();
		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter sum:");
		int sum=sc.nextInt();

		boolean ret=obj.ask(arr,sum);

		System.out.println(ret);
	}
}

import java.util.*;

class Demo{
	
	String prefix(String[] arr){
		
		Arrays.sort(arr);

		int count=0;
		for(int i=0;i<arr[0].length();i++){
			
			if(i==arr[arr.length-1].length() || arr[0].charAt(i) != arr[arr.length-1].charAt(i)){
				break;
			}
			if(arr[0].charAt(i) == arr[arr.length-1].charAt(i)){
				count++;
			}
		}

		return arr[0].substring(0,count);
	}
	public static void  main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Size of array:");
		int n=sc.nextInt();

		String arr[]=new String[n];


		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next();
		}

		String str=obj.prefix(arr);

		System.out.println(str);
	}
}

import java.util.*;

class Demo{
	
	long[] square(int arr[]){
		long arr2[]=new long[arr.length];
               
               for(int i=0;i<arr.length;i++){
                   long temp=1;
                   for(int j=0;j<arr.length;j++){
                       
                       if(i!=j){
                           temp=temp*arr[j];
                       }
                   }
                   arr2[i]=temp;
                   
               }
               
               return arr2;		
	}
	public static void  main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];


		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		long ret[]=obj.square(arr);

		System.out.println(Arrays.toString(ret));
	}
}

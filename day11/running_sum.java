import java.util.*;

class Demo{

	int[] running(int arr[]){
		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i]+arr[i-1];
		}
		return arr;
	}

	public static void main(String[]args){
		Demo obj=new Demo();

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int n=sc.nextInt();
		int arr[]=new int[n];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret[]=obj.running(arr);

		System.out.println(Arrays.toString(ret));
	}
}

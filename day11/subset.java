import java.util.*;

class Demo{

	String running(int a1[],int a2[]){
		Arrays.sort(a1);
      Arrays.sort(a2);
      
      int i=0;
      int j=0;
      int count=0;
      while(i<a2.length && j<a1.length){
          if(a2[i]==a1[j]){
              i++;
              j++;
              count++;
          }
          else{
              j++;
          }
      }
      if(count==a2.length){
          return "Yes";
      }
      else{
          return "No";
      }
	}

	public static void main(String[]args){
		Demo obj=new Demo();

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array1:");
		int m=sc.nextInt();
		int arr1[]=new int[m];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr1.length;i++){
			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter size of array2:");
		int n=sc.nextInt();
		int arr2[]=new int[n];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}

		String str=obj.running(arr1,arr2);

		System.out.println(str);
	}
}

import java.util.*;

class Demo{
	public int quadruple(int[] arr,int x,int y) {
        	int dist=arr.length;
		int xind=-1;
		int yind=-1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==x ){
				xind=i;
			}
			else if(arr[i]==y){
				yind=i;
			}
			if(xind !=-1 && yind!=-1){
				if(xind>yind && (xind-yind)<dist){
					dist=xind-yind;
				}
				else if(xind<yind && (yind-xind)<dist){
                                        dist=yind-xind;
                                }
			}
		}
		if(xind==-1 || yind==-1){
			return -1;
		}
		else{
			return dist;
		}
		
		
    	}

	public static void main(String []args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];
		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter x:");
                int x=sc.nextInt();
		System.out.println("Enter y:");
                int y=sc.nextInt();

		int ret=obj.quadruple(arr,x,y);

		System.out.println(ret);
	}
}

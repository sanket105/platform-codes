import java.util.*;

class Demo{
	public int[] plusOne(int[] arr) {
        
	if(arr[arr.length-1]!=9){
            arr[arr.length-1]=arr[arr.length-1]+1;
        }
        else{
            int flag=0;
            for(int i=0;i<arr.length;i++){
                if(arr[i]!=9){
                    flag=1;
                }
            }
            if(flag==0){
                int arr2[]=new int[arr.length+1];
                for(int i=arr2.length-1;i>=0;i--){
                        if(i==0){
                            arr2[i]=1;
			    break;
                        }
                        arr2[i]=0;
                }
                return arr2;
            }
          
            for(int i=arr.length-1;i>=0;i--){
                if(arr[i]==9){
                    arr[i]=0;
                }
                else{
                    arr[i]=arr[i]+1;
                    break;
                }
            }
        }
        return arr;
    }

	public static void main(String []args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];
		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret[]=obj.plusOne(arr);

		System.out.println(Arrays.toString(ret));
	}
}

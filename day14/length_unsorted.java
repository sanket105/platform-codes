import java.util.*;

class Demo{

	int[] intersection(int arr1[]){
		
		int arr2[]=new int[arr1.length];
		
		for(int i=0;i<arr2.length;i++){
			arr2[i]=arr1[i];
		}
		Arrays.sort(arr2);

		int arr3[]=new int[2];

		int flag=0;
		for(int i=0;i<arr1.length;i++){
			if(arr2[i]!=arr1[i] && flag==0){
				arr3[0]=i;
				flag=1;
			}
			if(arr2[i]!=arr1[i] && flag==1){
				arr3[1]=i;
			}
		}


		return arr3;
	}

	public static void main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter size of array:");
		int n=sc.nextInt();
		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret[]=obj.intersection(arr);

		System.out.println(Arrays.toString(ret));
	}
}

import java.util.*;

class Demo{

	int intersection(int arr[],int x,int n){
		 int start =0,end=1;
    int sum = arr[start];
    int result = Integer.MAX_VALUE;
    if(sum>x) return 1;
    
    if(end<n) sum +=arr[end];
    
    while(start<n && end <n){
        
        if(sum>x){
            result = Math.min(result,end-start+1);
            sum = sum-arr[start];
            start++;
        }else{
            end++;
            if(end<n){
                sum = sum + arr[end];
            }
        }
    }
    
    if(result == Integer.MAX_VALUE){
        return 0;
    }
    return result;

		
		
	}

	public static void main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter size of array:");
		int n=sc.nextInt();
		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter x:");
		int x=sc.nextInt();

		int ret=obj.intersection(arr,x,n);

		System.out.println(ret);
	}
}

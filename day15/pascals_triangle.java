import java.util.*;

class Demo{
	int[][] sort(int num){
		
		int arr[][]=new int[num][];

		for(int i=0;i<num;i++){
			arr[i]=new int[i];
			for(int j=0;j<i;j++){
				if(j==1){
					arr[i][j]=1;
				}
				else if(j==i){
					arr[i][j]=1;
				}
				else{
					arr[i][j]=i-1;
				}
			}
		}
		return arr;
	}

	public static void main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter numrows:");
		int n=sc.nextInt();
		
		

		int ret[][]=obj.sort(n);
		for(int i=0;i<ret[0].length;i++){
			for(int j=0;j<ret[i].length;j++){
				System.out.print(ret[i][j]+" ");
			}
		}
	}
}

import java.util.*;

class Demo{
	int[][] sort(int[][] arr){
		for(int i=0;i<arr[0].length;i++){
			for(int j=i;j<arr[i].length;j++){
				int temp=arr[i][j];
				arr[i][j]=arr[j][i];
				arr[j][i]=temp;
			}
		}

		return arr;
	}

	public static void main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter size of array:");
		int n=sc.nextInt();
		
		int arr[][]=new int[n][n];
		System.out.println("Enter array elements:");
		for(int i=0;i<arr[0].length;i++){
			for(int j=0;j<arr[i].length;j++){
			
				arr[i][j]=sc.nextInt();
			}
			
		}

		int ret[][]=obj.sort(arr);
		for(int i=0;i<ret[0].length;i++){
			for(int j=0;j<ret[i].length;j++){
				System.out.print(arr[i][j]+" ");
			}
		}
	}
}

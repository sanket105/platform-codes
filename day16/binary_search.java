import java.util.*;

class Demo{
	int stocks(int arr[],int x){
		
		int start=0;
		int end=arr.length-1;
		
		while(start<=end){
			int mid=(start+end)/2;
			if(mid==x){
				return mid-1;
			}
			else if(mid<x){
				start=mid+1;
			}
			else{
				end=mid-1;
			}
		}
		return -1;
	}

	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		Demo obj=new Demo();

		System.out.println("Enter length of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter element:");
		int x=sc.nextInt();

		int ret=obj.stocks(arr,x);
		System.out.println(ret);
		
	}
}

import java.util.*;

class Demo{
	int stocks(int arr[]){
		
		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		return max;
	}

	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		Demo obj=new Demo();

		System.out.println("Enter length of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.stocks(arr);
		System.out.println(ret);
		
	}
}

import java.util.*;

class Demo{
	int stocks(int arr[]){
		
		int min = Integer.MAX_VALUE;
		int minInd = -1;

		int max = Integer.MIN_VALUE;
		int maxInd = -1;

		int profit = Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){

			if(arr[i]<=min){
				min = arr[i];
				minInd = i;
				max = Integer.MIN_VALUE;
			}else if(arr[i]>=max){
				max = arr[i];
				maxInd = i;
			}
			if(maxInd>minInd && profit<max-min){
				profit=max-min;
			}
		}

		if(profit != Integer.MIN_VALUE){
			return profit;
		}else{
			return 0;
		}
	}

	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);

		Demo obj=new Demo();

		System.out.println("Enter length of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.stocks(arr);
		System.out.println(ret);
		
	}
}

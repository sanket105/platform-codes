import java.util.*;

class Demo{

        long[] xyz(long arr[]){
                long min=Long.MAX_VALUE;
                long min2=Long.MAX_VALUE;
                long max=Long.MIN_VALUE;

                Arrays.sort(arr);
                
                if(arr[0]==arr[arr.length-1]){
                        return new long[]{-1};
                }
                else{
                    min=arr[0];
                    int i=0;
                    while(arr[i]==arr[i+1]){
                        i++;
                    }
                    i++;
                    min2=arr[i];
                    long arr2[]=new long[]{min,min2};
                    return arr2;
                }
        }

        public static void main(String[]args){

                Scanner sc=new Scanner(System.in);

                Demo obj=new Demo();

                System.out.println("Enter size of array:");
                int n=sc.nextInt();
                long arr[]=new long[n];

                System.out.println("Enter array elements:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
               	}

                long ret[]=obj.xyz(arr);

                for(int i=0;i<ret.length;i++){
			System.out.print(ret[i]+" ");
		}
		System.out.println();

        }
}


import java.util.*;

class Demo{
	int count(int arr[],int sum){
		HashMap<Integer,Integer> hm=new HashMap<>();
		
		int cnt=0;
		for(int i=0;i<arr.length;i++){
			if(hm.containsKey(sum-arr[i])){
				cnt=cnt+hm.get(sum-arr[i]);
			}
			if(hm.containsKey(arr[i])){
				hm.put(arr[i],hm.get(arr[1])+1);
			}
			else{
				hm.put(arr[i],1);
			}
		
		}
		return cnt;
	}
 public static void main(String[]args){

                Scanner sc=new Scanner(System.in);

                Demo obj=new Demo();

                System.out.println("Enter size of array:");
                int n=sc.nextInt();
                int arr[]=new int[n];

                System.out.println("Enter array elements:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                
		System.out.println("Enter k:");
                int k=sc.nextInt();

                int ret=obj.count(arr,k);

                System.out.println(ret);

       }
}

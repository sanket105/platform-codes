import java.util.*;

class Demo{
	int majority(int arr[]){
		Arrays.sort(arr);
		int temp=(arr.length/2)+1;
		return arr[temp];

		
	}
 public static void main(String[]args){

                Scanner sc=new Scanner(System.in);

                Demo obj=new Demo();

                System.out.println("Enter size of array:");
                int n=sc.nextInt();
                int arr[]=new int[n];

                System.out.println("Enter array elements:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int ret=obj.majority(arr);

                System.out.println(ret);

       }
}

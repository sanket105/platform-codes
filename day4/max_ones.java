import java.util.*;

class Demo{

	int Ones(int arr[]){
		int count=0;
		int max=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==1){
				count++;
			}
			else{
				if(count>max){
					max=count;
				}
				count=0;
			}
		}
		if(count>max){
			max=count;
		}
		return max;
	}

	public static void main(String[]args){

                Scanner sc=new Scanner(System.in);

                Demo obj=new Demo();

                System.out.println("Enter size of array:");
                int n=sc.nextInt();
                int arr[]=new int[n];

                System.out.println("Enter array elements:");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int ret=obj.Ones(arr);

            
                System.out.println(ret);

        }
}

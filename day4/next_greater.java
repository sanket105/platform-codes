import java.util.*;

class Demo{

	int[] Greater(int arr1[],int arr2[]){
		HashMap<Integer,Integer> hm=new HashMap<>();
        
                for(int i=0;i<arr2.length;i++){
                        hm.put(arr2[i],i);
                }
                for(int i=0;i<arr1.length;i++){
                        int k=hm.get(arr1[i]);
                        int flag=0;
                        while(k<arr2.length){
                                if(arr2[k]>arr1[i]){
                                        arr1[i]=arr2[k];
                                        flag=1;
                                        break;
                                }
                                k++;
                        }
                        if(flag==0){
                                arr1[i]=-1;
                        }
                }
                return arr1;
	}

	public static void main(String[]args){

                Scanner sc=new Scanner(System.in);

                Demo obj=new Demo();

                System.out.println("Enter size of array:");
                int n=sc.nextInt();
                int arr1[]=new int[n];

                System.out.println("Enter array elements:");
                for(int i=0;i<arr1.length;i++){
                        arr1[i]=sc.nextInt();
                }
                System.out.println("Enter size of array:");
                int n2=sc.nextInt();
                int arr2[]=new int[n2];

                System.out.println("Enter array elements:");
                for(int i=0;i<arr2.length;i++){
                        arr2[i]=sc.nextInt();

                int ret[]=obj.Greater(arr1,arr2);

            
                for(int z:ret){
			System.out.println(z);
		}

        }
}
}

import java.util.*;

class Demo{

	int unique(int arr[],int n,int m){
		
		Arrays.sort(arr);

		int min=Integer.MAX_VALUE;
		for(int i=arr.length-1;i>=m-1;i--){
			int temp=arr[i]-arr[i-(m-1)];
			 
				//System.out.println("here1");

			if(temp<min){
				min=temp;
				//System.out.println("here2");
			}
		}

		return min;
	}

	public static void main(String args[]){
		Demo obj=new Demo();

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		 System.out.println("Enter no of students:");
                int m=sc.nextInt();

		int ret=obj.unique(arr,n,m);

		System.out.println(ret);
	}
}

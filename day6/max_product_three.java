import java.util.*;

class Demo{

	int unique(int arr[],int n){
		
		Arrays.sort(arr);
		int x=0;
		int y=1;
		int z=2;
		int max=Integer.MIN_VALUE;
		for(;z<arr.length;x++,y++,z++){
			int product=arr[x]*arr[y]*arr[z];
			if(product>max){
				max=product;
			}
		}
		 if(arr[0]<0 && arr[1]<0 && arr[arr.length-1]>0){
                    int prod=arr[0]*arr[1]*arr[arr.length-1];
                    if(max<prod){
                        max=prod;
                    }

                }

		
		return max;
	}

	public static void main(String args[]){
		Demo obj=new Demo();

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.unique(arr,n);

		System.out.println(ret);
	}
}

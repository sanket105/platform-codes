import java.util.*;

class Demo{

	int unique(int arr[],int n){
		
		int arr2[]=new int[n];

		arr2[0]=arr[0];
		int j=0;
		int count=1;

		for(int i=1;i<arr.length;i++){
			if(arr[i]!=arr2[j]){
				arr2[++j]=arr[i];
				count++;
			}
		}
		for(int i=0;i<arr2.length;i++){
			arr[i]=arr2[i];
		}

		return count;
	}

	public static void main(String args[]){
		Demo obj=new Demo();

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter array elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.unique(arr,n);

		System.out.println(ret);
	}
}

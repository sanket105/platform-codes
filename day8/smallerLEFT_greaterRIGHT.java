import java.util.*;
class Demo{
	
	int elem(int arr[]){
		
		int larr[]=new int[arr.length];
		int rarr[]=new int[arr.length];
		
		larr[0]=arr[0];
		for(int i=1;i<arr.length;i++){
			larr[i]=Math.max(larr[i-1],arr[i+1]);

		}

		System.out.println(Arrays.toString(larr));


		rarr[arr.length-1]=arr[arr.length-1];
		for(int i=arr.length-2;i>=0;i--){
			rarr[i]=Math.max(rarr[i+1],arr[i]);
		}
		System.out.println(Arrays.toString(rarr));

		for(int i=1;i<arr.length-1;i++){
			if(arr[i]>=larr[i] && arr[i]<=rarr[i]){
				return arr[i];
			}
		
		}
		return -1;
		
	}
	public static void main(String args[]){
                Demo obj=new Demo();

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter size of array:");
                int m=sc.nextInt();	

		int arr[]=new int[m];

                System.out.println("Enter array elements:");
                for(int i=0;i<m;i++){
                        arr[i]=sc.nextInt();
                }

            

                int ret=obj.elem(arr);
		System.out.println(ret);
        }
}

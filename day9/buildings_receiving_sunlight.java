import java.util.*;

class Demo{
	int elem(int arr[]){
		int max=Integer.MIN_VALUE;
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
				count++;
			}
		}

		return count;
	}

	public static void main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		

		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.elem(arr);
		System.out.println(ret);
	}
}

import java.util.*;

class Demo{
	int elem(int arr[]){
		int range=arr.length;
		int sum1=(range*(range+1))/2;
		int sum2=0;
		for(int i=0;i<arr.length;i++){
			sum2=sum2+arr[i];
		}
		int diff=sum1-sum2;
		return diff;
	}

	public static void main(String[]args){
		Demo obj=new Demo();
		Scanner sc=new Scanner(System.in);
		

		System.out.println("Enter size of array:");
		int n=sc.nextInt();

		int arr[]=new int[n];

		System.out.println("Enter elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int ret=obj.elem(arr);
		System.out.println(ret);
	}
}
